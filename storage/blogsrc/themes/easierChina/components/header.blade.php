<style>
    @font-face {
        font-family: 'AleoLight';
        src: url('/fonts/Aleo-Light.otf');
    }
    .header {
        width: 100%;
        height: 50px;
        background-color: white;
        box-shadow: 0 0 2px #313335;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;
        font-family: 'AleoLight';
    }
    .header .logo {
        margin-left: 20px;
        font-size: 1.5em;
    }
    .logo:hover {
        cursor: pointer;
        color: #0090F7;
    }
    .header .nav {
        margin-right: 20px;
        display: flex;
        flex-direction: row;
        align-items: center;

    }
    .header .nav .nav-item {
        margin-left: 60px;
        font-size: 1.4em;
    }
    .nav-item:hover {
        cursor: pointer;
        color: #0090F7;
    }
</style>
<script>
    function onNavClick(target, url) {
        switch (target) {
            case 'thispage': window.location.href = url; break;
            case 'newpage': window.open(url); break;
        }
    }
    function onTitleClick() {
        window.location.href = '/'
    }
</script>
<div class="header">
    <div class="logo" onclick="onTitleClick()">
        {{ $title }}
    </div>
    <div class="nav">
        @foreach ($nav as $_nav)
            <div class="nav-item" onclick="onNavClick('{{ $_nav->target }}', '{{ $_nav->prefix }}{{ $_nav->value }}')">{{ $_nav->title }}</div>
        @endforeach
    </div>
</div>
