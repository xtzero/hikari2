<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/source/{{ $logo }}" type="image/x-icon"/>
    <title>@yield('pageTitle', ''){{$title}} - {{$subTitle}}</title>
    <style>
        html, body, .container {
            margin: 0;
        }
        .container {
            width: 100%;
            height: 100vh;
            overflow: hidden;
            display: flex;
            flex-direction: column;
        }
        .content {
            width: 100%;
            height: calc(100% - 60px);
            position: relative;
        }
        .footer {
            position: absolute;
            left: 0;
            bottom: 5px;
            display: flex;
            flex-direction: row;
            justify-content: center;
            font-size: 13px;
            color: gray;
            width: 100%;
            height: 10px;
        }
    </style>
</head>
<body>
<div class="container">
    @include('components.header')
    <div class="content">
        @yield('content', '该页面无内容')
    </div>
    @include('components.footer')
</div>
</body>
</html>
