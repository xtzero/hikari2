<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('pageTitle', ''){{$title}} - {{$subTitle}}</title>
    <link rel="shortcut icon" href="/source/{{ $logo }}" type="image/x-icon"/>
    <style>
        html, body {
            margin: 0;
        }
    </style>
</head>
<body>
    @yield('content', '该页面无内容')
</body>
</html>
