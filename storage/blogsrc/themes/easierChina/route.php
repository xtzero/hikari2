<?php
use Illuminate\Support\Facades\Route;

require_once 'ThemeController.php';
Route::get('/', [ThemeController::class, 'index']);
Route::get('/archive', [ThemeController::class, 'archive'])->name('文章列表');
Route::get('/category', [ThemeController::class, 'category']);
Route::get('/post/{id}', [ThemeController::class, 'post'])->name('hikari.default.post');
Route::get('/p/{id}', [ThemeController::class, 'page'])->name('hikari.default.page');

