@extends('layouts.homepage_mobile')
@section('content')
<style>
    .indexmobile_container {
        display: flex;
        flex-direction: column;
        align-items: center;
        background-color: #efeff5;
        width: 100%;
        height: 100vh;
        overflow-x: hidden;
        overflow-y: scroll;
    }
    .indexmobile_container .title {
        width: 100%;
        height: 66px;
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;
        background-color: #fff;
        padding: 10px 0;
    }
    .indexmobile_container .title .logo {
        width: 44px;
        height: 44px;
        background-size: cover;
        background-position: center;
        margin-left: 20px;
    }
    .indexmobile_container .title .worklog {
        letter-spacing: 7px;
        margin-right: 20px;
        font-size: 1.7em;
        font-weight: 500;
    }
    .indexmobile_container .banner {
        width: 100%;
        height: 200px;
        background-size: cover;
        background-position: center;
        background-image: url('{{ $j['homepage_mobile']['banner_image'] }}');
        color: {{ $j['homepage_mobile']['banner_title_color'] }};
        line-height: 200px;
        text-align: center;
        font-size: 1.5em;
    }
    .indexmobile_container .list {
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: center;
    }
    .indexmobile_container .list .row {
        width: 96%;
        background-color: white;
        border-radius: 5px;
        margin-bottom: 10px;
    }
    .indexmobile_container .list .row .row-title {
        font-size: 20px;
        font-weight: 500;
        margin: 10px 20px;
    }
    .indexmobile_container .list .row .row-content {
        font-size: 15px;
        margin: 10px 20px;
        overflow-x: hidden;
    }
    .indexmobile_container .list .row .row-user {
        width: 100%;
        text-align: left;
        margin-left: 20px;
        font-size: 14px;
        color: gray;
        margin-bottom: 10px;
    }
    .indexmobile_container .list .row .row-user .category {
        margin-right: 10px;
    }
    .indexmobile_container .list .row .row-user .create_time {
        margin-right: 10px;
    }
</style>
<div class="indexmobile_container">
    <div class="title">
        <div class="logo" style="background-image: url('/source/{{ $logo }}')"></div>
        <div class="worklog">{{ $j['homepage_mobile']['title'] }}</div>
    </div>
    <div class="banner">
        {{ $j['homepage_mobile']['banner_title'] }}
    </div>
    <style>
        .nav {
            width: calc(96% - 20px);
            height: 44px;
            margin: 5px 10px;
            display: flex;
            flex-direction: row;
            align-items: center;
            padding: 10px 10px;
            flex-wrap: nowrap;
        }
        .nav .nav-item {
            margin-right: 10px;
            font-size: 13px;
            background-color: #fff;
            border-radius: 5px;
            padding: 5px 10px;
        }
        .footer {
            width: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;
            font-size: 8px;
            color: gray;
            margin: 20px 0;
        }
        .footer .footer-row {
            flex-wrap: nowrap;
            display: inline;
            width: auto;
        }
    </style>
    <div class="nav">
        <script>
            function jump(url, newTab = false) {
                if (newTab) {
                    window.open(url)
                } else {
                    window.location.href = url
                }
            }
        </script>
        @foreach($j['homepage_mobile']['nav'] as $v)
            <div class="nav-item" onclick="jump('{{ $v['url'] }}')">{{ $v['title'] }}</div>
        @endforeach
    </div>
    <div class="list">
        <script>
            function jumpToDetail(url) {
                @if($archives['archiveOpenBlogInNewTab'])
                    window.open(url)
                @else
                    window.location.href = url
                @endif
            }
        </script>
        @foreach($archives['posts'] as $v)
            <div class="row" onclick="jumpToDetail('/post/{{ $v->id }}', true)">
                <div class="row-title">{{ $v->title }}</div>
                <div class="row-content">
                    {!! $v->thumb !!}
                </div>
                <div class="row-user">
                    <span class="category">{{ $v->category_name }}</span>
                    <span class="create_time">{{ $v->create_time }}</span>
                </div>
            </div>
        @endforeach
    </div>
    @include('components.footer')
</div>
@endsection
