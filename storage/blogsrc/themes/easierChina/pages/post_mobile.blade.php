@extends('layouts.homepage_mobile')
@section('pageTitle')
    {{$post->title}} |
@endsection
@section('content')
    @include('components.content_html_css')
    <style>
        .mobile_container {
            width: 100vw;
            height: 100vh;
            overflow-x: hidden;
            overflow-y: scroll;
            background-color: #efeff5;
            display: flex;
            flex-direction: column;
            align-items: center;
        }
        .mobile_container .card {
            width: 90%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            background-color: white;
            /*margin-top: 20px;*/
            border-radius: 10px;
            padding: 20px 0;
            margin-bottom: 20px;
        }
        .mobile_container .card .title {
            width: calc(100% - 60px);
            font-size: 25px;
            font-weight: 400;
            margin: 0 30px 20px 30px;
            word-break: break-all;
        }
        .mobile_container .card .props {
            width: 100%;
            text-align: left;
            margin-left: 60px;
            font-size: 14px;
            color: gray;
            display: flex;
            flex-direction: row;
        }
        .mobile_container .card .props .category {
            margin-right: 10px;
        }
        .mobile_container .card .props .create_time {
            margin-right: 10px;
        }
        .mobile_container .card .content_html {
            width: calc(100% - 60px);
            font-size: 14px;
            margin: 20px 0px 20px 0px;
            word-break: break-word;
        }

        .footer {
            width: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;
            font-size: 8px;
            color: gray;
            margin: 20px 0;
        }
        .footer .footer-row {
            flex-wrap: nowrap;
            display: inline;
            width: auto;
        }
    </style>
    <div class="mobile_container">
        <style>
            .nav {
                width: calc(96% - 20px);
                height: 44px;
                margin: 5px 10px;
                display: flex;
                flex-direction: row;
                align-items: center;
                padding: 10px 10px;
                flex-wrap: nowrap;
            }
            .nav .nav-item {
                margin-right: 10px;
                font-size: 13px;
                background-color: #fff;
                border-radius: 5px;
                padding: 5px 10px;
            }
        </style>
        <div class="nav">
            <script>
                function jump(url, newTab = false) {
                    if (newTab) {
                        window.open(url)
                    } else {
                        window.location.href = url
                    }
                }
            </script>
            <div class="nav-item" onclick="jump('/')"> ◀ </div>
            @foreach($j['homepage_mobile']['nav'] as $v)
                <div class="nav-item" onclick="jump('{{ $v['url'] }}')">{{ $v['title'] }}</div>
            @endforeach
        </div>
        <div class="card">
            <div class="title">{{ $post->title }}</div>
            <div class="props">
                @if($post->category_name)
                    <div class="category">{{$post->category_name}}</div>
                @endif
                <div class="create_time" id="createTime">{{$post->create_time}}</div>
            </div>
            <div class="content_html">
                {!! $post->content_html !!}
            </div>
        </div>
        @include('components.footer')

    </div>
@endsection
