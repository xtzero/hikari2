@extends('layouts.homepage')
@section('content')
    <style>
        body{
            background-color: #FDFDFD;
        }
        .section {
            width: 100vw;
            height: 100vh;
        }
        .section1 {
            position: relative;
            display: flex;
            flex-direction: row;
            justify-content: center;
            align-items: center;
        }
        .section1 > .photo-left, .section1 > .photo-right {
            width: 18vw;
            height: 70vh;
            background-size: cover;
            background-position: center;
            position: absolute;
            box-shadow: 0 0 10px #666;
        }
        .section1 > .photo-left {
            background-image: url('{{ $j['homepage']['image_left']  }}');
            top: 20vh;
            left: 44px;
        }
        .section1 > .photo-right {
            background-image: url('{{ $j['homepage']['image_right']  }}');
            right: 44px;
            top: 0;
        }
        .section1 > .section1-text {
            display: flex;
            flex-direction: column;
            width: calc(64vw - 88px - 4vw);
            margin-left: 2vw;
        }
        .section1 > .section1-text > span {
            font-family: 'AleoLight';
        }
        .section1 > .section1-text > span:nth-child(1) {
            color: #404040;
            font-size: 63px;
        }
        .section1 > .section1-text > span:nth-child(2) {
            color: #666666;
            font-size: 28px;
        }
    </style>

    <div class="section section1">
        <div class="photo-left"></div>
        <div class="photo-right"></div>
        <div class="section1-text">
            <span>{!! $j['homepage']['desc1'] !!}</span>
            <span>{!! $j['homepage']['desc2'] !!}</span>
        </div>
    </div>
@endsection
