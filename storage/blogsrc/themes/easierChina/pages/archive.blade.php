@extends('layouts.main')
@section('pageTitle', '文章目录 | ')
@section('content')
    <style>
        .footer {
            @if($blogUsePage == 1 && ceil($count / $blogPageLimit) > 1)
                margin-top: 0px;
            @else
                margin-top: 50px;
            @endif
        }
        .pagetitle {
            font-size: 2em;
            margin-left: 10vw;
            margin-top: 20px;
            font-family: 'AleoLight';
        }
        .archives {
            margin-left: 10vw;
            margin-top: 5vh;
        }
        .archive {
            margin-bottom: 12px;
            display: flex;
            flex-direction: row;
            align-items: center;
        }
        .category-text {
            width: auto;
            border: 1px solid #0090FB;
            padding: 2px 5px;
            border-radius: 10px;
            font-size: 13px;
            color: #0090FB;
            display: inline-block;
        }
        .title {
            font-size: 18px;
            font-weight: 500;
            margin-left: 20px;
        }
        .title:hover {
            text-decoration: underline;
            color: #3496D8;
            cursor: pointer;
        }
        .create_time {
            margin-left: 50px;
            font-size: 13px;
            color: gray;
        }
        td {
            height: 30px;
        }
        .pages {
            margin: 30px 0 10px 10vw;
            display: flex;
            flex-direction: row;
            align-items: center;
        }
        .page {
            margin-right: 20px;
        }
        .currPage {
            text-decoration: underline;
            color: #3498DB;
            font-weight: 500;
        }
        .page:hover {
            cursor: pointer;
            text-decoration: underline;
        }
        .page-tip {
            margin-right: 20px;
        }
    </style>
    <script>
        function getQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]); return '';
        }
        function openBlog(id) {
            if ('{{$archiveOpenBlogInNewTab}}' == '1') {
                window.open(`/post/${id}`)
            } else {
                window.location.href = `/post/${id}`
            }
        }
        function goPage(page) {
            const currPage = '{{$page}}' - 0
            const maxPage = Math.ceil('{{$count / $blogPageLimit}}' - 0)

            switch (page) {
                case 'first': {
                    page = '1'
                } break
                case 'prev': {
                    if (currPage > 1) {
                        page = `${currPage - 1}`
                    } else {
                        return false
                    }
                } break
                case 'after': {
                    if (currPage < maxPage) {
                        page = currPage + 1
                    } else {
                        return false
                    }
                } break
                case 'last': {
                    page = maxPage
                } break
            }

            // 处理其他参数
            const category = getQueryString('category')
            window.location.href = `/archive?page=${page}&category=${category}`
        }
    </script>
    <div class="pagetitle">
        文章目录
        @if ($category)
            · {{$posts[0]->category_name}}
        @endif
        · Page {{$page}}
    </div>
    <div class="archives">
        <table>
            @foreach($posts as $post)
                <tr>
                    <td>
                        <div class="category-text">{{$post->category_name}}</div>
                    </td>
                    <td>
                        <div class="title" onclick="openBlog('{{$post->id}}')">{{$post->title}}</div>
                    </td>
                    <td>
                        <div class="create_time">{{$post->create_time}}</div>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    @if($blogUsePage == 1 && ceil($count / $blogPageLimit) > 1)
    <div class="pages">
        @if($page > 1)
            <div class="page" onclick="goPage('first')">
                <<
            </div>
        @endif
        @if($page > 1)
            <div class="page" onclick="goPage('prev')">
                <
            </div>
        @endif
        @for ($i = 0; $i < ceil($count / $blogPageLimit); $i ++)
            <div class="page @if($page == ($i + 1)) currPage @endif" onclick="goPage('{{$i + 1}}')">
                {{$i + 1}}
            </div>
        @endfor
        @if($page < ceil($count / $blogPageLimit))
            <div class="page" onclick="goPage('after')">
                >
            </div>
        @endif
        @if($page < ceil($count / $blogPageLimit))
            <div class="page" onclick="goPage('last')">
                >>
            </div>
        @endif
        <div class="page-tip">
            共 {{$count}} 篇
        </div>
        <div class="page-tip">
            每页 {{$blogPageLimit}} 篇
        </div>
    </div>
    @endif
@endsection
