@extends('layouts.main')
@section('pageTitle')
{{$post->title}} |
@endsection
@section('content')
    @include('components.content_html_css')
    <style>
        .content {
            width: 80vw;
            overflow-y: hidden;
            padding-left: 10vw;
            margin-top: 50px;
        }
        .content > .title {
            font-size: 3em;
            letter-spacing: 3px;
            font-weight: 500;
        }
        .content > .props {
            display: flex;
            flex-direction: row;
            align-items: center;
            margin-top: 10px;
        }
        .content > .props > .category {
            border: 1px solid #0090FB;
            padding: 2px 5px;
            border-radius: 10px;
            font-size: 13px;
            color: #0090FB;
        }
        .content > .props > .create_time {
            margin-left: 20px;
        }
        .content > .props > .busuanzi {
            margin-left: 20px;
        }
        .content > .content_html {
            margin-top: 100px;
        }
        .content > .busuanzi {
            margin-left: 20px;
        }
        .content > .content_html {
            width: 80vw;
        }
        @media screen and (max-width: 500px) {
            .content > .props > .category {
                font-size: 8px;
            }
            .content > .props > .create_time {
                font-size: 8px;
            }
            .content > .props > .busuanzi {
                font-size: 8px;
            }
        }
    </style>
    <div class="title">{{$post->title}}</div>
    <div class="props">
        @if($post->category_name)
            <div class="category">{{$post->category_name}}</div>
        @endif
        <div class="create_time" id="createTime">{{$post->create_time}}</div>
    </div>
    <div class="content_html">
        {!!$post->content_html!!}
    </div>
@endsection
