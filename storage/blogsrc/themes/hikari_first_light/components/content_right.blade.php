<style>
    .section {
        width: calc(100% - 40px);
        height: auto;
        background-color: #fff;
        padding: 20px;
        border-radius: 10px;
        box-shadow: 0 0 30px hsl(0deg 0% 78% / 25%);
        margin-bottom: 20px;
    }
    .section .section-title {
        font-size: 16px;
        font-weight: 300;
    }
    .section .section-content {
        font-size: 14px;
        font-weight: 300;
        margin-top: 5px;
    }
</style>
<div class="content-right">
    <div class="section">
        <div class="section-title">{{ $title }}</div>
        <div class="section-content">{{ $subTitle }}</div>
    </div>
    @foreach($j['sections'] as $v)
        <div class="section">
            @if($v['title'])
                <div class="section-title">{!! $v['title'] !!}</div>
            @endif
            @if($v['content'])
                <div class="section-content">{!! $v['content'] !!}</div>
            @endif
        </div>
    @endforeach
    <!-- <iframe frameborder="no" border="0" marginwidth="0" marginheight="0" width=330 height=86 src="//music.163.com/outchain/player?type=2&id=1414002887&auto=1&height=66"></iframe> -->
    @include('components.footer')
</div>

