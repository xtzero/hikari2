<style>
    .footer {
        display: flex;
        flex-direction: column;

    }
    .footer a {
        color: #808080;
    }
    .footer .footer-section {
        font-size: 12px;
        color: #808080;
        margin-bottom: 10px;
        font-weight: 300;
    }
</style>
<div class="footer">
    @foreach($j['footer'] as $v)
        <div class="footer-section">{!! $v !!}</div>
    @endforeach
</div>
