<style>
    .left {
        margin-left: 50px;
    }
    .right {
        margin-right: 50px;
        display: flex;
        flex-direction: row;
        height: 100%;
    }
    .menu-item {
        margin-left: 40px;
        font-size: 14px;
        font-weight: 300;
        height: 100%;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        text-align: center;
    }
    .menu-item:hover {
        cursor: pointer;
        color: #4091F7;
    }
    .menu-item-active {
        border-bottom: solid 2px #4091F7;
    }
    .head-title {
        font-size: 1.4em;
        font-weight: 200;
    }
    .head-title:hover {
        cursor: pointer;
    }
    @media screen and (max-width: 500px) {
        .left {
            margin-left: 5vw;
        }
        .right {
            margin-right: 5vw;
            display: flex;
            flex-direction: row;
            height: 100%;
        }
        .menu-item {
            margin-left: 20px;
            font-size: 14px;
            font-weight: 300;
            height: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            text-align: center;
        }
        .menu-item:hover {
            cursor: pointer;
            color: #4091F7;
        }
        .menu-item-active {
            border-bottom: solid 2px #4091F7;
        }
        .head-title {
            font-size: 1em;
            font-weight: 200;
        }
        .head-title:hover {
            cursor: pointer;
        }
    }
</style>
<script>
    function onNavClick(target, url) {
        switch (target) {
            case 'thispage': window.location.href = url; break;
            case 'newpage': window.open(url); break;
        }
    }
    function onTitleClick() {
        window.location.href = '/'
    }
</script>
<div class="left">
    <div class="head-title" onclick="onTitleClick()">{{$title}}</div>
</div>
<div class="right">
    @foreach ($nav as $_nav)
        <div class="menu-item{{ $_nav->curr ? ' menu-item-active' : ''}}" onclick="onNavClick('{{$_nav->target}}', '{{$_nav->prefix}}{{$_nav->value}}')">{{$_nav->title}}</div>
    @endforeach
</div>
