<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('pageTitle', ''){{$title}} - {{$subTitle}}</title>
    <script async="" src="//busuanzi.ibruce.info/busuanzi/2.3/busuanzi.pure.mini.js"></script>
    <style>
        html, body, .container {
            margin: 0;
        }
        .container {
            width: 100%;
            min-height: 100vh;
            display: flex;
            flex-direction: column;
            background-color: #FAFAFA;
        }
        .header {
            width: 100%;
            height: 64px;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            align-items: center;
            box-shadow: 0 0 20px 0 rgb(0 0 0 / 5%);
        }
        .content {
            width: 100%;
            position: relative;
            display: flex;
            flex-direction: row;
            margin-top: 20px;
        }
        .content .content-left {
            margin-left: 30px;
            width: 70%;
            min-height: calc(100vh - 120px);
        }
        .content .content-right {
            margin-right: 30px;
            margin-left: 30px;
            width: 20%;
        }
        @media screen and (max-width: 500px) {
            .header {
                height: 40px;
                display: flex;
                flex-direction: row;
                justify-content: space-between;
                align-items: center;
                box-shadow: 0 0 20px 0 rgb(0 0 0 / 5%);
            }
            .container {
                padding-bottom: 20vh;
            }
            .content {
                width: 100%;
                position: relative;
                display: flex;
                flex-direction: column;
                margin-top: 20px;
            }
            .content .content-left {
                margin-left: 30px;
                margin-right: 30px;
                width: calc(100% - 60px);
                min-height: calc(100vh - 120px);
            }
            .content .content-right {
                margin-left: 30px;
                margin-right: 30px;
                width: calc(100% - 60px);
            }
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="header">
            @include('components.header')
        </div>

        <div class="content">
            <div class="content-left">
                @yield('content', '该页面无内容')
            </div>
            @include('components.content_right')
        </div>
    </div>
</body>
</html>
