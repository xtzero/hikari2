<?php

class ThemeController extends \App\Http\Controllers\Controller
{
    public function index(\Illuminate\Http\Request $request)
    {
        $settings = (new \App\Db\Setting())->getSettingDir($request->siteId);
        $themeJson = json_decode($settings["theme_json_{$settings['theme']}"], true);
//        var_dump($this->archive($request, true));
        return view('pages.index', [
            'title' => $settings['title'] ?? '',
            'nav' => $this->isCurrNav(json_decode($settings['nav']) ?? []),
            'footer' => $settings['footer'],
            'logo' => $settings['logo'],
            'subTitle' => $settings['subTitle'],
            'j' => $themeJson,
            'archives' => $this->archive($request, true)
        ]);
    }

    public function archive(\Illuminate\Http\Request $request, $returnData = false)
    {
        $settings = (new \App\Db\Setting())->getSettingDir($request->siteId);
        $themeJson = json_decode($settings["theme_json_{$settings['theme']}"], true);
        $db = \Illuminate\Support\Facades\DB::table('blog')->where([
            'is_del' => 0,
            'site' => $request->siteId
        ]);
        if ($request->input('category') && $request->input('category') > 0) {
            $db = $db->where(['category' => $request->input('category')]);
        }
        $count = $db->count();
        if ($settings['blogUsePage'] == 1 && $settings['blogPageLimit'] > 0) {
            $limit = $settings['blogPageLimit'];
            $page = $request->input('page', 1);
            $db = $db->skip(($page - 1) * $limit)->limit($limit);
        }
        $res = $db->orderByDesc('create_time')->get();
        $categorys = array_column(\Illuminate\Support\Facades\DB::table('category')->whereIn('id', array_column($res->toArray(), 'category'))->get()->toArray(), null, 'id');
        foreach ($res as &$v) {
            $v->category_name = isset($v->category) && isset($categorys[$v->category]) ? $categorys[$v->category]->name : '';
        }
        $resData = [
            'logo' => $settings['logo'],
            'title' => $settings['title'],
            'subTitle' => $settings['subTitle'],
            'nav' => $this->isCurrNav(json_decode($settings['nav']) ?? []),
            'footer' => $settings['footer'],
            'posts' => $res,
            'blogUsePage' => $settings['blogUsePage'],
            'blogPageLimit' => $settings['blogPageLimit'],
            'page' => $page ?? false,
            'count' => $count ?? 0,
            'archiveOpenBlogInNewTab' => $settings['archiveOpenBlogInNewTab'],
            'category' => $request->input('category', ''),
            'j' => $themeJson
        ];
        return $returnData ? $resData : view('pages.archive', $resData);
    }

    public function post($id, \Illuminate\Http\Request $request)
    {
        $settings = (new \App\Db\Setting())->getSettingDir($request->siteId);
        $themeJson = json_decode($settings["theme_json_{$settings['theme']}"], true);
        $settingArr = [
            'logo' => $settings['logo'],
            'title' => $settings['title'],
            'subTitle' => $settings['subTitle'],
            'nav' => $this->isCurrNav(json_decode($settings['nav']) ?? []),
            'footer' => $settings['footer'],
            'j' => $themeJson
        ];
        if (!$id) {
            return view('pages.404', array_merge($settingArr, [
                'errCode' => '500',
                'message' => '没有文章 id'
            ]));
        }
        $res = \Illuminate\Support\Facades\DB::table('blog')
            ->orWhere(['id' => $id, 'url' => $id])->where(['is_del' => 0, 'site' => $request->siteId])->first();
        if (!$res) {
            return view('pages.404', array_merge($settingArr, [
                'errCode' => '404',
                'message' => '该文章不存在'
            ]));
        }

        $category = isset($res->category) ?
            \Illuminate\Support\Facades\DB::table('category')->where(['id' => $res->category])->first(['name'])->name ?? ''
            : '';
        $res->category_name = $category;
        return view('pages.post', array_merge($settingArr, [
            'post' => $res
        ]));
    }

    public function page($id, \Illuminate\Http\Request $request)
    {
        $settings = (new \App\Db\Setting())->getSettingDir($request->siteId);
        $themeJson = json_decode($settings["theme_json_{$settings['theme']}"], true);
        $settingArr = [
            'logo' => $settings['logo'],
            'title' => $settings['title'],
            'subTitle' => $settings['subTitle'],
            'nav' => $this->isCurrNav(json_decode($settings['nav']) ?? []),
            'footer' => $settings['footer'],
            'j' => $themeJson
        ];
        if (!$id) {
            return view('pages.404', array_merge($settingArr, [
                'errCode' => '500',
                'message' => '无页面 id'
            ]));
        }
        $res = \Illuminate\Support\Facades\DB::table('page')
            ->orWhere(['id' => $id, 'url' => $id])->where(['is_del' => 0, 'site' => $request->siteId])->first();
        if (!$res) {
            return view('pages.404', array_merge($settingArr, [
                'errCode' => '404',
                'message' => '该页面不存在'
            ]));
        }

        $res->category_name = '';
        return view('pages.post', array_merge($settingArr, [
            'post' => $res
        ]));
    }

    public function category(\Illuminate\Http\Request $request)
    {
        $settings = (new \App\Db\Setting())->getSettingDir($request->siteId);
        $themeJson = json_decode($settings["theme_json_{$settings['theme']}"], true);
        $settingArr = [
            'logo' => $settings['logo'],
            'title' => $settings['title'],
            'subTitle' => $settings['subTitle'],
            'nav' => $this->isCurrNav(json_decode($settings['nav']) ?? []),
            'footer' => $settings['footer'],
            'j' => $themeJson
        ];
        $res = (new \App\Db\Columns())->getColumns(true, $request->siteId);

        return view('pages.category', array_merge($settingArr, [
            'categorys' => $res
        ]));
    }

    private function isCurrNav($nav): array
    {
        $currNav = $_SERVER['SERVER_NAME'];
        if (!in_array($_SERVER['SERVER_PORT'], ['80', '443'])) {
            $currNav .= ':' . $_SERVER['SERVER_PORT'];
        }
        $currNav .= $_SERVER['REQUEST_URI'];
        if (substr($currNav, -1, 1) !== '/') {
            $currNav .= '/';
        }

        foreach ($nav as $v) {
            $navUrl = str_replace(['http://', 'https://'], '', $v->prefix . $v->value);
            if (substr($navUrl, -1, 1) !== '/') {
                $navUrl .= '/';
            }
            if ($navUrl == $currNav) {
                $v->curr = true;
            } else {
                $v->curr = false;
            }
        }
        return $nav;
    }
}
