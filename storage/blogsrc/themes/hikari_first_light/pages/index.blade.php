@extends('layouts.main')
@section('content')
    <style>
        .index-content {
            margin-left: 50px;
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 0 0 30px hsl(0deg 0% 78% / 25%);
            padding: 40px 50px;
            margin-bottom: 20px;
            display: flex;
            flex-direction: column;
            color: #000000A6;
        }
        .index-content .title {
            font-size: 3em;
            font-weight: 300;
            display: flex;
            flex-direction: row;
            align-items: center;
        }
        /*.index-content .title .title-tag {*/
        /*    background-color: #0090F7;*/
        /*    height: 24px;*/
        /*    padding: 5px 10px;*/
        /*    color: white;*/
        /*    display: flex;*/
        /*    flex-direction: row;*/
        /*    align-items: center;*/
        /*    justify-content: center;*/
        /*    font-size: 20px;*/
        /*    margin-right: 20px;*/
        /*    border-radius: 5px;*/
        /*}*/
        .index-content .subtitle {
            font-size: 1.5em;
            font-weight: 200;
            margin-top: 20px;
        }

        .index-post {
            margin-left: 50px;
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 0 0 30px hsl(0deg 0% 78% / 25%);
            padding: 40px 50px;
            margin-bottom: 20px;
            display: flex;
            flex-direction: column;
        }
        .index-post .title {
            font-size: 40px;
            font-weight: 300;
            display: flex;
            flex-direction: row;
            align-items: center;
            cursor: pointer;
            text-decoration: none;
            color: #000000A6;
        }
        .index-post .content {
            font-size: 14px;
            width: calc(100% - 40px);
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            max-height: 30vh;
            overflow: hidden;
            padding-left: 40px;
            border-left: solid 5px #595959;
        }
        .index-post .comments {
            display: flex;
            flex-direction: row;
            color: #595959;
            font-size: 12px;
            margin-top: 40px;
        }
        .index-post .comments .comments-item {
            margin-right: 20px;
        }
        .index-post .comments .comments-item-mobile {
            margin-right: 20px;
            display: none;
        }
        
        .index-post .comments .comments-item .comments-blod {
            font-weight: 500;
            text-decoration: none;
        }
        .tip {
            font-weight: 400;
            margin: 50px 0 20px 20px;
        }
        @media screen and (max-width: 500px) {
            .index-content,
            .index-post {
                margin-left: 0;
            }

            .index-content .title {
                font-size: 2em;
            }
            .index-content .subtitle {
                font-size: 1.2em;
            }
            .index-post .title {
                font-size: 20px;
            }
            .index-post .content {
                display: none;
            }
            .index-post .comments .comments-item {
                display: none;
            }
            .index-post .comments .comments-item-mobile {
                display: inline-block;
            }
    </style>
    <div class="index-content">
        <div class="title">
            欢迎访问 {{ $title }} !
        </div>
        <div class="subtitle">
            「{!! $subTitle !!}」
        </div>
    </div>
    <div class="tip">最近文章</div>
    @foreach($archives['posts'] as $v)
        <div class="index-post">
            <a class="title" href="/post/{{ $v->id }}">{{ $v->title }}</a>
            <div class="content">{{ strip_tags($v->content_html) }}</div>
            <div class="comments">
                <div class="comments-item">发表时间 <span class="comments-blod">{{ $v->create_time }}</span></div>
                <div class="comments-item">发表在 <a href="/archive?category={{ $v->category }}" class="comments-blod">{{ $v->category_name }}</a></div>
                <div class="comments-item">阅读数 <span class="comments-blod">{{ $v->view_count }}</span></div>
                <div class="comments-item-mobile"><span class="comments-blod">{{ $v->create_time }}</span></div>
                <div class="comments-item-mobile"><a href="/archive?category={{ $v->category }}" class="comments-blod">{{ $v->category_name }}</a></div>
            </div>
        </div>
    @endforeach

@endsection
