@extends('layouts.main')
@section('pageTitle')
{{$post->title}} |
@endsection
@section('content')
    @include('components.content_html_css')
    <style>
        .content-left {
            width: calc(70% - 100px) !important;
            background-color: white;
            padding: 50px;
            border-radius: 10px;
            font-weight: 300;
        }
        .content-left .title {
            font-size: 35px;
            font-weight: 400;
        }
        .content-left .props {
            display: flex;
            flex-direction: row;
            align-items: center;
        }
        .content-left .props .category {
            color: #3D8BEA;
            margin-right: 20px;
        }
        .content-left .props .create_time {

        }
        .content-left .content_html {
            word-break: break-word;
            margin-top: 70px;
        }
        @media screen and (max-width: 500px) {
            .content {
                width: 100%;
                position: relative;
                display: flex;
                flex-direction: column;
                margin-top: 20px;
                overflow-x: hidden;
            }
            .content .content-left {
                margin-left: 20px;
                margin-right: 20px;
                width: calc(100% - 80px) !important;
                min-height: calc(100vh - 120px);
                padding: 20px;
            }
            .content-left .title {
                font-size: 20px;
                font-weight: 400;
            }
            .content-left .props {
                display: flex;
                flex-direction: row;
                align-items: center;
                margin-top: 10px;
            }
            .content-left .content_html {
                margin-top: 30px;
            }
            .content .content-right {
                margin-top: 20px;
                margin-left: 20px;
                margin-right: 20px;
                width: calc(100% - 40px) !important;
            }
        }
    </style>
{{--    <script src="http://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>--}}
    <div class="title">{{$post->title}}</div>
    <div class="props">
        @if($post->category_name)
            <div class="category">{{$post->category_name}}</div>
        @endif
        <div class="create_time" id="createTime">{{$post->create_time}}</div>
    </div>
    <div class="content_html">
        {!!$post->content_html!!}
    </div>
    <script>
        // let createTimeClicked = false
        // document.getElementById('createTime').addEventListener('click', () => {
        //     if (!createTimeClicked) {
        //         createTimeClicked = true
        //     } else {
        //         document.getElementsByClassName('header')[0].style.display = 'none'
        //         document.getElementsByClassName('footer-police')[0].style.display = 'none'
        //         document.getElementsByClassName('footer-author')[0].innerHTML = '<span style="font-size: 10px; color: gray;">使用 Hikari 2 生成</span>'
        //         document.getElementsByClassName('content')[0].style.marginTop = '100px'
        //
        //         html2canvas(document.querySelector(".container")).then(canvas => {
        //             document.getElementsByClassName('container')[0].innerHTML = ''
        //             document.getElementsByClassName('container')[0].appendChild(canvas)
        //
        //             alert('已创建长图，刷新页面可恢复')
        //         })
        //     }
        // })
    </script>
@endsection
