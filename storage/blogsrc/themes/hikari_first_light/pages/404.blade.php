@extends('layouts.main')
@section('pageTitle', '404 | ')
<style>
    .content {
        height: calc(100vh - 66px);
        display: flex;
        flex-direction: column;
        align-items: center;
        position: relative;
    }
    .footer {
        position: absolute;
        bottom: 5px;
    }
    .pagetitle {
        width: 100%;
        text-align: center;
        letter-spacing: 20px;
        font-size: 8em;
        margin-top: 20vh;
    }
    .message {
        font-size: 15px;
        color: gray;
    }
</style>
@section('content')
    <div class="pagetitle">
        @if(isset($errCode) && $errCode > 0)
            {{$errCode}}
        @else
            404
        @endif
    </div>
    <div class="message">
        @if(isset($message) && $message != '')
            {{$message}}
        @else
            出错了，并且没有错误信息
        @endif
    </div>
@endsection
