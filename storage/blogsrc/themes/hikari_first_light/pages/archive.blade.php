@extends('layouts.main')
@section('pageTitle', '文章目录 | ')
@section('content')
    <style>
        .content-left {
            width: calc(100% - 50px);
            font-weight: 300;
        }
        .index-post {
            margin-left: 50px;
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 0 0 30px hsl(0deg 0% 78% / 25%);
            padding: 40px 50px;
            margin-bottom: 20px;
            display: flex;
            flex-direction: column;
        }
        .index-post .title {
            font-size: 40px;
            font-weight: 300;
            display: flex;
            flex-direction: row;
            align-items: center;
            cursor: pointer;
            text-decoration: none;
            color: #000000A6;
        }
        .index-post .content {
            font-size: 14px;
            width: calc(100% - 40px);
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            max-height: 30vh;
            overflow: hidden;
            padding-left: 40px;
            border-left: solid 5px #595959;
        }
        .index-post .comments {
            display: flex;
            flex-direction: row;
            color: #595959;
            font-size: 12px;
            margin-top: 40px;
        }
        .index-post .comments .comments-item {
            margin-right: 20px;
        }
        .index-post .comments .comments-item-mobile {
            display: none;
        }
        .index-post .comments .comments-item .comments-blod {
            font-weight: 500;
            text-decoration: none;
        }
        .index-post .comments .comments-item .comments-category {
            text-decoration: underline;
        }
        .pagetitle {
            padding: 20px 50px;
        }
        .pagetitle .title {
            font-size: 30px;
        }
        .pagetitle .comments {
            font-size: 18px;
            margin-top: 10px;
        }
        .pagination {
            padding: 10px 50px;
        }
        .pagination .comments {
            display: flex;
            flex-direction: row;
            align-items: center;
            margin-top: 0;
        }
        .pagination .page-item {
            cursor: pointer;
            text-decoration: underline;
        }
        .pagination .currPage {
            font-weight: 500;
        }
        @media screen and (max-width: 500px) {
            .content {
                width: 100%;
                position: relative;
                display: flex;
                flex-direction: column;
                margin-top: 20px;
                overflow-x: hidden;
            }
            .content .content-left {
                margin-left: 20px;
                margin-right: 20px;
                width: calc(100% - 40px) !important;
                min-height: calc(100vh - 120px);
            }
            .content .content-right {
                margin-left: 20px;
                margin-right: 20px;
                width: calc(100% - 80px) !important;
            }
            .index-post {
                margin-left: 0;
            }
            .index-post .title {
                font-size: 20px;
            }
            .index-post .content {
                display: none;
            }
            .index-post .comments .comments-item {
                display: none;
            }
            .index-post .comments {
                justify-content: space-between;
            }
            .index-post .comments .comments-item-mobile {
                display: inline-block;
            }
            .index-post.pagination .comments {
                justify-content: flex-start;
            }
            .index-post.pagination .comments-item {
                display: inline-block;
                margin-right: 10px;
            }
        }
    </style>
    <script>
        function getQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]); return '';
        }
        function openBlog(id) {
            if ('{{$archiveOpenBlogInNewTab}}' == '1') {
                window.open(`/post/${id}`)
            } else {
                window.location.href = `/post/${id}`
            }
        }
        function goPage(page) {
            const currPage = '{{$page}}' - 0
            const maxPage = Math.ceil('{{$count / $blogPageLimit}}' - 0)

            switch (page) {
                case 'first': {
                    page = '1'
                } break
                case 'prev': {
                    if (currPage > 1) {
                        page = `${currPage - 1}`
                    } else {
                        return false
                    }
                } break
                case 'after': {
                    if (currPage < maxPage) {
                        page = currPage + 1
                    } else {
                        return false
                    }
                } break
                case 'last': {
                    page = maxPage
                } break
            }

            // 处理其他参数
            const category = getQueryString('category')
            window.location.href = `/archive?page=${page}&category=${category}`
        }
    </script>
    <div class="pagetitle index-post">
        <div class="title">文章目录 第 {{$page}} 页</div>
        @if ($category)
            <div class="comments">
                <div class="comments-item">来自分类：{{$posts[0]->category_name}}</div>
            </div>
        @endif
    </div>
    @foreach($posts as $post)
        <div class="index-post">
            <a class="title" href="" onclick="openBlog('{{$post->id}}')">{{ $post->title }}</a>
            <div class="content">{{ strip_tags($post->content_html) }}</div>
            <div class="comments">
                <div class="comments-item">发表时间 <span class="comments-blod">{{ $post->create_time }}</span></div>
                <div class="comments-item">发表在 <a href="/archive?category={{ $post->category }}" class="comments-blod comments-category">{{ $post->category_name }}</a></div>
                <div class="comments-item">阅读数 <span class="comments-blod">{{ $post->view_count }}</span></div>
                <div class="comments-item-mobile"><span class="comments-blod">{{ $post->create_time }}</span></div>
                <div class="comments-item-mobile"><a href="/archive?category={{ $post->category }}" class="comments-blod comments-category">{{ $post->category_name }}</a></div>
                <div class="comments-item-mobile">阅读数 <span class="comments-blod">{{ $post->view_count }}</span></div>
            </div>
        </div>
    @endforeach
    @if($blogUsePage == 1 && ceil($count / $blogPageLimit) > 1)
        <div class="index-post pagination">
            <div class="comments">
                @if($page > 1)
                    <div class="comments-item page-item" onclick="goPage('first')"> << </div>
                @endif
                @if($page > 1)
                    <div class="comments-item page-item" onclick="goPage('prev')"> < </div>
                @endif
                @for ($i = 0; $i < ceil($count / $blogPageLimit); $i ++)
                    <div class="comments-item @if($page == ($i + 1)) currPage @else page-item @endif" onclick="goPage('{{$i + 1}}')"> {{$i + 1}} </div>
                @endfor
                @if($page < ceil($count / $blogPageLimit))
                    <div class="comments-item page-item" onclick="goPage('after')"> > </div>
                @endif
                @if($page < ceil($count / $blogPageLimit))
                    <div class="comments-item page-item" onclick="goPage('last')"> >> </div>
                @endif
                <div class="comments-item"> 共 {{$count}} 篇 </div>
                <div class="comments-item"> 每页 {{$blogPageLimit}} 篇 </div>
            </div>
        </div>
    @endif
@endsection
