<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('pageTitle', ''){{$title}} - {{$subTitle}}</title>
    <script async="" src="//busuanzi.ibruce.info/busuanzi/2.3/busuanzi.pure.mini.js"></script>
    <style>
        html, body, .container {
            margin: 0;
        }
        .container {
            width: 100%;
            min-height: 100vh;
            display: flex;
            flex-direction: column;
        }
        .header {
            width: 100%;
            height: 66px;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            align-items: center;
        }
        .content {
            width: 100%;
            position: relative;
        }
        .footer {
            width: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="header">
            @include('components.header')
        </div>
        
        <div class="content">
            @yield('content', '该页面无内容')
            <div class="footer">
                @include('components.footer')
            </div>
        </div>
    </div>
</body>
</html>