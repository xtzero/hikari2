<style>
    .left {
        margin-left: 30px;
    }
    .right {
        margin-right: 30px;
        display: flex;
        flex-direction: row;
    }
    .menu-item {
        margin-left: 20px;
    }
    .menu-item:hover {
        cursor: pointer;
        text-decoration: underline;
        color: #3498DB;
    }
    .head-title {
        font-size: 1.4em;
        font-weight: 600;
    }
    .head-title:hover {
        cursor: pointer;
    }
</style>
<script>
    function onNavClick(target, url) {
        switch (target) {
            case 'thispage': window.location.href = url; break;
            case 'newpage': window.open(url); break;
        }
    }
    function onTitleClick() {
        window.location.href = '/'
    }
</script>
<div class="left">
    <div class="head-title" onclick="onTitleClick()">{{$title}}</div>
</div>
<div class="right">
    @foreach ($nav as $_nav)
        <div class="menu-item" onclick="onNavClick('{{$_nav->target}}', '{{$_nav->prefix}}{{$_nav->value}}')">{{$_nav->title}}</div>
    @endforeach
</div>