<style>
    .content_html h1, h2, h3, h4, h5, h6 {
        color: black;
    }

    .content_html h1::before {
        content: '# ';
        color: #0090FB;
    }
    .content_html h2::before {
        content: '## ';
        color: #0090FB;
    }
    .content_html h3::before {
        content: '### ';
        color: #0090FB;
    }
    .content_html h4::before {
        content: '#### ';
        color: #0090FB;
    }
    .content_html h5::before {
        content: '##### ';
        color: #0090FB;
    }
    .content_html h6::before {
        content: '###### ';
        color: #0090FB;
    }

    /* 这段代码可以这么写，但是太丑了 */
    /* @for($i = 1; $i <= 6; $i ++)
        .content_html h{{$i}}::before {
            content: '{{str_repeat('#', $i)}} ';
        }
    @endfor */

    .content_html > p {
        font-size: 1em;
        letter-spacing: 1px;
        line-height: 25px;
        margin-bottom: 30px;
    }

    .content_html blockquote {
        display: flex;
        flex-direction: column;
        justify-content: center;
        border-radius: 5px;
        border-left: solid 10px rgba(0, 144, 247, 0.8);
        background-color: rgba(0, 144, 247, 0.2);
        padding: 10px 20px;
        padding-left: 15px;
    }
    @font-face {
        font-family: 'consolas';
        src: url('/fonts/consola.ttf');
    }
    .content_html a {
        text-decoration: none;
        margin: 0 3px;
        color: #158BF1;
    }
    .content_html img {
        max-width: 100%;
        -webkit-box-shadow: 0 0 30px #ccc;
        box-shadow: 0 0 30px #ccc;
    }

    /* 大块的代码块 */
    .content_html pre {
        background-color: black;
        padding: 10px 20px;
        line-height: 30px;
        border-radius: 10px;
    }
    .content_html pre code {
        color: white;
        font-family: consolas;
    }


    /* 行内代码块 */
    .content_html p code, .content_html li>code{
        color: rgba(0,144,247, 1);
        background-color: rgba(0,144,247,0.2);
        margin: 0 5px;
        padding: 3px 10px;
        border-radius: 3px;
        font-family: consolas;
    }

    .content_html table {
        margin: 20px 0;
    }
    .content_html table th, td{
        border-bottom: solid 1px lightgray;
        padding: 5px 10px;
        text-align: left;
        margin-right: 200px;
    }

     /*列表 */
    .content_html ol li {
        margin-bottom: 10px;
    }
</style>