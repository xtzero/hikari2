@extends('layouts.main')
@section('content')
    <style>
        .content {
            width: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;
            height: calc(100vh - 66px);
        }
        .footer {
            position: absolute;
            bottom: 10px;
        }
        .logo {
            margin-top: 10vh;
        }
        .logo>img {
            width: 10vw;
            height: 10vw;
            border-radius: 50%;
        }
        .title {
            font-size: 55px;
            font-weight: 500;
            letter-spacing: 2px;
            margin-top: 100px;
        }
        .subtitle {
            margin-top: 10px;
            font-size: 30px;
        }
    </style>
    <div class="logo">
        <img src="/source/{{$logo}}" alt="">
    </div>
    <div class="title">
        {{$title}}
    </div>
    <div class="subtitle">
        {{$subTitle}}
    </div>
@endsection
