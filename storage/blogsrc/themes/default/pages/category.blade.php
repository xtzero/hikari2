@extends('layouts.main')
@section('pageTitle', '分类 | ')
@section('content')
    <style>
        .content {
            min-height: calc(100vh - 100px);
            position: relative;
            width: 100vw;
        }
        .footer {
            margin-top: 50px;
        }
        .pagetitle {
            font-size: 2em;
            margin-left: 10vw;
            margin-top: 20px;
        }
        .categorys {
            margin-left: 10vw;
            margin-top: 50px;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
        }
        .category {
            width: 120px;
            height: 130px;
            border: solid 1px black;
            margin: 0 10px 20px 0;
            padding: 20px 30px;
        }
        .category:hover {
            border-color: #0090F7;
            cursor: pointer;
            color: #0090F7;
        }
        .category .name {
            font-size: 1.5em;
            font-weight: 500;
        }
        .category .count {
            margin-top: 40px;
        }
        .category .create_time {
            margin-top: 5px;
            font-size: 13px;
            color: gray;
        }
    </style>
    <script>
        function jumpToArchives(id) {
            window.location.href = `/archive?category=${id}`
        }
    </script>
    <div class="pagetitle">分类</div>
    <div class="categorys">
        @foreach ($categorys as $category)
            @if ($category->count > 0)
                <div class="category" onclick="jumpToArchives('{{$category->id}}')">
                    <div class="name">{{$category->name}}</div>
                    <div class="count">{{$category->count}} 篇文章</div>
                    <div class="create_time">创建于 {{date('Y-m-d', strtotime($category->create_time))}}</div>
                </div>
            @endif
        @endforeach
    </div>
@endsection
