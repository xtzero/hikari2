@extends('layouts.main')
@section('pageTitle')
{{$post->title}} |
@endsection
@section('content')
    @include('components.content_html_css')
    <style>
        .content {
            width: 80vw;
            overflow-y: hidden;
            padding-left: 10vw;
            margin-top: 50px;
        }
        .content > .title {
            font-size: 3em;
            letter-spacing: 3px;
            font-weight: 500;
        }
        .content > .props {
            display: flex;
            flex-direction: row;
            align-items: center;
            margin-top: 10px;
        }
        .content > .props > .category {
            border: 1px solid #0090FB;
            padding: 2px 5px;
            border-radius: 10px;
            font-size: 13px;
            color: #0090FB;
        }
        .content > .props > .create_time {
            margin-left: 20px;
        }
        .content > .props > .busuanzi {
            margin-left: 20px;
        }
        .content > .content_html {
            margin-top: 100px;
        }
        .content > .busuanzi {
            margin-left: 20px;
        }
        .content > .content_html {
            width: 80vw;
        }
        @media screen and (max-width: 500px) {
            .content > .props > .category {
                font-size: 8px;
            }
            .content > .props > .create_time {
                font-size: 8px;
            }
            .content > .props > .busuanzi {
                font-size: 8px;
            }
        }
    </style>
    <script src="http://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
    <div class="title">{{$post->title}}</div>
    <div class="props">
        @if($post->category_name)
            <div class="category">{{$post->category_name}}</div>
        @endif
        <div class="create_time" id="createTime">{{$post->create_time}}</div>
    </div>
    <div class="content_html">
        {!!$post->content_html!!}
    </div>
    <script>
        let createTimeClicked = false
        document.getElementById('createTime').addEventListener('click', () => {
            if (!createTimeClicked) {
                createTimeClicked = true
            } else {
                document.getElementsByClassName('header')[0].style.display = 'none'
                document.getElementsByClassName('footer-police')[0].style.display = 'none'
                document.getElementsByClassName('footer-author')[0].innerHTML = '<span style="font-size: 10px; color: gray;">使用 Hikari 2 生成</span>'
                document.getElementsByClassName('content')[0].style.marginTop = '100px'
                
                html2canvas(document.querySelector(".container")).then(canvas => {
                    document.getElementsByClassName('container')[0].innerHTML = ''
                    document.getElementsByClassName('container')[0].appendChild(canvas)

                    alert('已创建长图，刷新页面可恢复')
                })
            }
        })
    </script>
@endsection
