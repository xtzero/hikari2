<?php

use Illuminate\Support\Str;

return [
    'default' => 'sqlite',
    'connections' => [
        'sqlite' => [
            'driver' => 'sqlite',
            'url' => '',
            'database' => str_replace('config', 'storage/blogsrc/db.sqlite', __DIR__),
            'prefix' => 'hikari2_',
            'foreign_key_constraints' => true,
        ],
    ],

    'migrations' => 'migrations'
];
