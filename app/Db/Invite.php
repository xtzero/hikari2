<?php

namespace App\Db;

use App\Utils\Date;
use Illuminate\Support\Facades\DB;

class Invite
{
    public const Invite = 'invite';

    public function addInvite($data)
    {
        return DB::table(self::Invite)->insert([
            ...$data,
            'is_del' => 0,
            'create_time' => Date::now()
        ]);
    }

    public function editInvite($data)
    {
        return DB::table(self::Invite)->where([
            'id' => $data['id']
        ])->update($data);
    }

    public function addGuest()
    {

    }
}
