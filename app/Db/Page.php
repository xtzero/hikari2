<?php

namespace App\Db;

use App\Utils\Date;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Page
{
    public const PAGE = 'page';

    public function addNewPage($data, $siteId)
    {
        return DB::table(self::PAGE)->insertGetId([
            'site' => $siteId,
            'title' => $data['title'],
            'content_md' => $data['content_md'],
            'content_html' => $data['content_html'],
            'url' => $data['url'] ?? '',
            'create_time' => $data['create_time'] ?? Date::now(),
            'is_del' => 0
        ]);
    }

    public function savePage($data, $siteId)
    {
        $d = [
            'site' => $siteId,
            'title' => $data['title'],
            'content_md' => $data['content_md'],
            'content_html' => $data['content_html'],
            'url' => $data['url'] ?? '',
            'update_time' => Date::now()
        ];
        if (isset($data['create_time']) && $data['create_time']) {
            $d['create_time'] = $data['create_time'];
        }
        return DB::table(self::PAGE)->where(['id' => $data['id']])->update($d);
    }

    public function getPageList($params, $siteId)
    {
        $whereArr = [
            'is_del' => 0,
            'site' => $siteId
        ];
        $limit = 20;
        if (isset($params['limit']) && $params['limit'] > 0) {
            $limit = $params['limit'];
        }
        $page = 1;
        if (isset($params['page']) && $params['page'] > 0) {
            $page = $params['page'];
        }
        list($data, $count) = [
            DB::table(self::PAGE)->where($whereArr)->select(['id', 'title', 'url', 'create_time'])->skip(($page - 1) * $limit)->limit($limit)->orderByDesc('create_time')->get()->toArray(),
            DB::table(self::PAGE)->where($whereArr)->count()
        ];

        return [
            'count' => $count,
            'data' => $data
        ];
    }

    public function deletePage($id)
    {
        return DB::table(self::PAGE)->where(['id' => $id])->update([
            'is_del' => 1,
            'update_time' => Date::now()
        ]);
    }

    public function getPageRow($id)
    {
        return DB::table(self::PAGE)->where(['id' => $id, 'is_del' => 0])->first();
    }
}
