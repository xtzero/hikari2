<?php

namespace App\Db;

use App\Utils\Date;
use Illuminate\Support\Facades\DB;

class Source
{
    public const TABLE = 'source';

    public function upload($base64)
    {
        $id = md5(time() . rand(100000, 999999));

        return [
            'id' => $id,
            'dbRes' => DB::table(self::TABLE)->insert([
                'id' => $id,
                'base64' => $base64,
                'create_time' => Date::now(),
                'is_del' => 0
            ])
        ];
    }

    public function getSource($id)
    {
        $dbRes = DB::table(self::TABLE)->where(['id' => $id, 'is_del' => 0])->first('base64');
        if ($dbRes) {
            return $dbRes->base64;
        } else {
            return false;
        }
    }
}
