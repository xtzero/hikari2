<?php

namespace App\Db;

use App\Utils\HikariEncode;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AdminPassword
{
    private static $dbFilePath = 'storage/blogsrc/adminpwd.hikari';

    /**
     * 设置密码
     * @param $newPwd
     */
    public static function setPassword($newPwd)
    {
        DB::table('adminpwd')->update(['pwd' => $newPwd, 'token' => md5($newPwd)]);
    }

    /**
     * 获取密码
     * @return false|string
     */
    public static function getPassword()
    {
        return DB::table('adminpwd')->select(['pwd'])->get()->toArray()[0]->pwd ?? '';
    }

    public static function getToken()
    {
        return DB::table('adminpwd')->select(['token'])->get()->toArray()[0]->token ?? '';
    }
}
