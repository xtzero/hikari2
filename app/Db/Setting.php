<?php

namespace App\Db;

use App\Utils\Date;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Setting
{
    public const SETTING = 'setting';
    public const VISIT_LOG = 'visitlog';

    public function setSetting($data, $siteId)
    {
        DB::table(self::SETTING)->whereIn('type', array_column($data, 'type'))->where(['site' => $siteId])->delete();
        return DB::table(self::SETTING)->insert(array_map(function($v) use($siteId) {
            $v['site'] = $siteId;
            $v['value'] = $v['value'] ?? '';
            $v['create_time'] = Date::now();
            return $v;
        }, $data));
    }

    public function getSetting($data, $siteId)
    {
        $db = DB::table(self::SETTING)->where(['site' => $siteId]);
        if (isset($data['types'])) {
            $db = $db->whereIn('type', explode(',', $data['types']));
        }
        return $db->get();
    }

    public function getSettingDir($siteId)
    {
        $setting = $this->getSetting([], $siteId);
        $res = [];
        foreach ($setting as $v) {
            $res[$v->type] = $v->value ?? '';
        }
        $settingDefaultValue = [
            'title' => '',
            'subTitle' => '',
            'logo' => '',
            'blogUsePage' => false,
            'blogPageLimit' => 0,
            'archiveOpenBlogInNewTab' => true,
            'nav' => '',
            'footer' => '',
            'theme' => 'default',
        ];
        foreach ($settingDefaultValue as $k => $v) {
            if (!isset($res[$k])) {
                $res[$k] = $v;
            }
        }
        return $res;
    }

    public function getVisitLogOptions()
    {
        $columns = [
            'path',
            'route'
        ];
        $res = [];
        foreach ($columns as $v) {
            $res[$v] = array_column(DB::table(self::VISIT_LOG)->distinct()->get($v)->toArray(), $v);
        }
        return $res;
    }

    public function getVisitLog($params)
    {
        $whereArr = [];
        if (isset($params['path']) && $params['path']) {
            $whereArr['path'] = $params['path'];
        }
        if (isset($params['route']) && $params['route']) {
            $whereArr['route'] = $params['route'];
        }
        $db = DB::table(self::VISIT_LOG);
        $count = $db->count();
        if ($count > 0) {
            if (!empty($whereArr)) {
                $db = $db->where($whereArr);
            }
            if (isset($params['page']) && isset($params['limit']) && $params['limit'] > 0 && $params['page'] > 0) {
                $db = $db->skip(($params['page'] - 1) * $params['limit'])->limit($params['limit']);
            }
            $db = $db->orderByDesc('create_time')->get()->toArray();
        } else {
            $db = [];
        }

        return [
            'count' => $count,
            'data' => $db
        ];
    }

    public function clearVisitLog($params)
    {
        $db = DB::table(self::VISIT_LOG);
        if (!isset($params['range'])) {
            $db = $db->where('create_time', '>', '1970-01-01');
        }
        switch ($params['range']) {
            case 'day': $db = $db->where('create_time', '>', \date('Y-m-d 00:00:00')); break;
            case 'month': $db = $db->where('create_time', '>', \date('Y-m-01 00:00:00')); break;
            case 'year': $db = $db->where('create_time', '>', \date('Y-01-01 00:00:00')); break;
            case 'all': $db = $db->where('create_time', '>', '1970-01-01 00:00:00'); break;
        }
        return $db->delete();
    }
}
