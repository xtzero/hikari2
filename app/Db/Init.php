<?php

namespace App\Db;

use Illuminate\Support\Facades\DB;

class Init
{
    public static function handle()
    {
        DB::table('adminpwd')->delete();
        DB::table('adminpwd')->insert([
            'pwd' => 123456,
            'token' => md5(123456)
        ]);
    }
}
