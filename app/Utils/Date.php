<?php

namespace App\Utils;

class Date
{
    public static function now($format = 'Y-m-d H:i:s')
    {
        return date($format, time());
    }
}
