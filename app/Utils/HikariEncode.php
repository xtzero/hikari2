<?php
namespace App\Utils;

class HikariEncode {

    private const SPLITER = '--__--__-_-';
    public static function encode(string $v): string
    {
        return base64_encode(implode(self::SPLITER, [
            time(),
            base64_encode($v),
            env('HIKARI_ENCODE_KEY')
        ]) . env('HIKARI_ENCODE_KEY')) . env('HIKARI_ENCODE_KEY');
    }

    public static function decode(string $v): string
    {
        return base64_decode(explode(self::SPLITER, str_replace(env('HIKARI_ENCODE_KEY'), '', base64_decode($v)))[1]);
    }
}
