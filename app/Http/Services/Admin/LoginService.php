<?php

namespace App\Http\Services\Admin;

use App\Db\AdminPassword;

class LoginService
{
    public static function resetPwd()
    {
        fwrite(STDOUT, "请输入新密码: ");
        $newPwd = str_replace("\n", '', fgets(STDIN));

        fwrite(STDOUT, "请重复新密码: ");
        $newPwd2 = str_replace("\n", '', fgets(STDIN));

        if ($newPwd != $newPwd2) {
            fwrite(STDOUT, "两遍密码输入不一致！" . PHP_EOL);
            return false;
        }

        fwrite(STDOUT, "验证成功，正在设置..." . PHP_EOL);
        AdminPassword::setPassword($newPwd);
        fwrite(STDOUT, "设置完毕！" . PHP_EOL);
        return true;
    }

    public function login($params)
    {
        $correctPwd = AdminPassword::getPassword();
        if ($correctPwd !== $params['pwd']) {
            return response()->json([
                'data' => [],
                'code' => 500,
                'msg' => '密码不正确'
            ]);
        }

        return response()->json([
            'data' => [
                'token' => AdminPassword::getToken()
            ],
            'code' => 200,
            'msg' => '登录成功'
        ]);
    }
}
