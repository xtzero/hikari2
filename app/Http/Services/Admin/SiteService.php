<?php

namespace App\Http\Services\Admin;

use App\Utils\Date;
use Illuminate\Support\Facades\DB;

class SiteService
{
    public static function init()
    {
        fwrite(STDOUT, "请输入域名: ");
        $domain = str_replace("\n", '', fgets(STDIN));
        DB::table('site')->delete();
        DB::table('site')->insert([
            'name' => '主站',
            'host' => $domain,
            'create_time' => Date::now()
        ]);
        fwrite(STDOUT, "完成");
    }
}
