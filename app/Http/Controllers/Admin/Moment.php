<?php

namespace App\Http\Controllers\Admin;

use App\Db\Columns;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Moment extends Controller
{
    public function sendMoment(Request $request)
    {
        $content = $request->input('content');
        if (empty($content)) {
            return response()->json([
                'code' => 500,
                'msg' => '请输入内容',
                'data' => []
            ]);
        }
        $emoji = $request->input('emoji') ?? '';

        return response()->json([
            'code' => 200,
            'msg' => '',
            'data' => DB::table('moments')->insert([
                'content' => $content,
                'emoji' => $emoji,
                'create_time' => date('Y-m-d H:i:s'),
                'is_del' => 0
            ])
        ]);
    }

    public function listMoments(Request $request)
    {
        $page = $request->input('page') ?? 1;
        $limit = $request->input('limit') ?? 20;

        return response()->json([
            'code' => 200,
            'msg' => '',
            'data' => DB::table('moments')->where([
                'is_del' => 0
            ])->skip(($page - 1) * $limit)->limit($limit)
            ->orderByDesc('create_time')->get()
        ]);
    }

    public function deleteMoment(Request $request)
    {
        $id = $request->input('id');
        if (!$id) {
            return response()->json([
                'code' => 500,
                'msg' => '请传入 id',
                'data' => []
            ]);
        }
        return response()->json([
            'code' => 200,
            'msg' => '',
            'data' => DB::table('moments')->where([
                'id' => $id
            ])->update([
                'is_del' => 1
            ])
        ]);
    }
}
