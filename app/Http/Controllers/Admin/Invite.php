<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Client\Request;

class Invite extends Controller
{
    /**
     * @api 新建邀请
     * @param String title Required 邀请名
     * @param String date Required 日期
     * @param String content unRequired 内容
     * @param int theme Required 主题
     */
    public function addInvite(Request $request)
    {
        return response()->json([
            'code' => 200,
            'msg' => '',
            'data' => (new \App\Db\Invite())->addInvite($request->params)
        ]);
    }
    /**
     * @api 修改邀请
     * @param int id Required 邀请id
     * @param String title Required 邀请名
     * @param String date Required 日期
     * @param String content Required 内容
     * @param int theme Required 主题
     */
    public function editInvite(Request $request)
    {
        return response()->json([
            'code' => 200,
            'msg' => '',
            'data' => (new \App\Db\Invite())->editInvite($request->params)
        ]);
    }

    /**
     * @api 邀请添加用户
     * @param int id Required 邀请id
     * @param String name Required 被邀请的用户名
     */
    public function addGuest(Request $request)
    {

    }

    /**
     * 邀请删除用户
     */

    /**
     * 邀请列表
     */
    /**
     * 邀请详情
     * 返回邀请的基本属性和用户列表
     */
    /**
     * 删除邀请
     */
    /**
     * 添加主题
     */
    /**
     * 删除主题
     */
}
