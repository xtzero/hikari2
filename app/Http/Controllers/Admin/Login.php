<?php
namespace App\Http\Controllers\Admin;

use App\Http\Services\Admin\LoginService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class Login extends Controller
{
    public function login(Request $req)
    {
        $pwd = $req->input('pwd');
        if (!$pwd) {
            return response()->json([
                'code' => 500,
                'msg' => '请输入密码'
            ]);
        }
        return (new LoginService())->login(['pwd' => $pwd]);
    }
}
