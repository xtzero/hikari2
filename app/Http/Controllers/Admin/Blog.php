<?php

namespace App\Http\Controllers\Admin;

use App\Db\Columns;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Blog extends Controller
{
    public function addCategory(Request $req)
    {
        if (!$req->input('name')) {
            return response()->json([
                'code' => 500,
                'msg' => '请输入标题'
            ]);
        }
        return response()->json([
            'code' => 200,
            'msg' => '',
            'data' => $req->input('id') ? (new Columns())->saveColumn($req->input(null), $req->siteId) : (new Columns())->addColumn($req->input('name'), $req->siteId)
        ]);
    }

    public function getCategoryList(Request $request)
    {
        return response()->json([
            'code' => 200,
            'msg' => '',
            'data' => (new Columns())->getColumns(true, $request->siteId) ?? []
        ]);
    }

    public function addNewBlog(Request $request)
    {
        if (!$request->input('title')) {
            return response()->json([
                'code' => 500,
                'msg' => '缺少标题',
                'data' => []
            ]);
        }
        if (!$request->input('category')) {
            return response()->json([
                'code' => 500,
                'msg' => '请选择分类',
                'data' => []
            ]);
        }
        if (!$request->input('content_md')) {
            return response()->json([
                'code' => 500,
                'msg' => '缺少 markdown 内容',
                'data' => []
            ]);
        }
        if (!$request->input('content_html')) {
            return response()->json([
                'code' => 500,
                'msg' => '缺少 html 内容',
                'data' => []
            ]);
        }
        return response()->json([
            'code' => 200,
            'msg' => '',
            'data' => $request->input('id') ? (new \App\Db\Blog())->saveBlog($request->input(null), $request->siteId) : (new \App\Db\Blog())->addNewBlog($request->input(null), $request->siteId)
        ]);
    }

    public function getBlogList(Request $request)
    {
        return response()->json([
            'code' => 200,
            'msg' => '',
            'data' => (new \App\Db\Blog())->getBlogList($request->input(null), $request->siteId)
        ]);
    }

    public function saveBlog(Request $request)
    {
        if (!$request->input('id')) {
            return response()->json([
                'code' => 500,
                'msg' => '缺少 id',
                'data' => []
            ]);
        }
        return $this->addNewBlog($request);
    }

    public function deleteBlog(Request $request)
    {
        if (!$request->input('id')) {
            return response()->json([
                'code' => 500,
                'msg' => '缺少 id',
                'data' => []
            ]);
        }
        return response()->json([
            'code' => 200,
            'msg' => '',
            'data' => (new \App\Db\Blog())->deleteBlog($request->input('id'))
        ]);
    }

    public function deleteCategory(Request $request)
    {
        if (!$request->input('id')) {
            return response()->json([
                'code' => 500,
                'msg' => '缺少 id',
                'data' => []
            ]);
        }
        return response()->json([
            'code' => 200,
            'msg' => '',
            'data' => (new \App\Db\Columns())->deleteCategory($request->input('id'))
        ]);
    }

    public function saveCategory(Request $request)
    {
        if (!$request->input('id')) {
            return response()->json([
                'code' => 500,
                'msg' => '缺少 id',
                'data' => []
            ]);
        }
        return $this->addCategory($request);
    }

    public function getBlogRow(Request $request)
    {
        if (!$request->input('id')) {
            return response()->json([
                'code' => 500,
                'msg' => '缺少 id',
                'data' => []
            ]);
        }

        return response()->json([
            'code' => 200,
            'msg' => '',
            'data' => (new \App\Db\Blog())->getBlogRow($request->input('id'))
        ]);
    }
}
