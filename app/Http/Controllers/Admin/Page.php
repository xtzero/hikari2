<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Page extends Controller
{
    public function addNewPage(Request $request)
    {
        if (!$request->input('title')) {
            return response()->json([
                'code' => 500,
                'msg' => '缺少标题',
                'data' => []
            ]);
        }
        if (!$request->input('content_md')) {
            return response()->json([
                'code' => 500,
                'msg' => '缺少 markdown 内容',
                'data' => []
            ]);
        }
        if (!$request->input('content_html')) {
            return response()->json([
                'code' => 500,
                'msg' => '缺少 html 内容',
                'data' => []
            ]);
        }
        return response()->json([
            'code' => 200,
            'msg' => '',
            'data' => $request->input('id') ? (new \App\Db\Page())->savePage($request->input(null), $request->siteId) : (new \App\Db\Page())->addNewPage($request->input(null), $request->siteId)
        ]);
    }

    public function getPageList(Request $request)
    {
        return response()->json([
            'code' => 200,
            'msg' => '',
            'data' => (new \App\Db\Page())->getPageList($request->input(null), $request->siteId)
        ]);
    }

    public function savePage(Request $request)
    {
        if (!$request->input('id')) {
            return response()->json([
                'code' => 500,
                'msg' => '缺少 id',
                'data' => []
            ]);
        }
        return $this->addNewPage($request);
    }

    public function deletePage(Request $request)
    {
        if (!$request->input('id')) {
            return response()->json([
                'code' => 500,
                'msg' => '缺少 id',
                'data' => []
            ]);
        }
        return response()->json([
            'code' => 200,
            'msg' => '',
            'data' => (new \App\Db\Page())->deletePage($request->input('id'))
        ]);
    }

    public function getPageRow(Request $request)
    {
        if (!$request->input('id')) {
            return response()->json([
                'code' => 500,
                'msg' => '缺少 id',
                'data' => []
            ]);
        }

        return response()->json([
            'code' => 200,
            'msg' => '',
            'data' => (new \App\Db\Page())->getPageRow($request->input('id'))
        ]);
    }
}
