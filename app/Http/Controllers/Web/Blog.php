<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class Blog extends Controller
{
    public function index()
    {
        return view('Web.index', [
            'blogTitle' => env('BLOG_TITLE')
        ]);
    }

    public function blog($id)
    {
        $blog = DB::table(\App\Db\Blog::BLOG)
            ->select()
            ->where(function($query) use($id) {
                $query->where(['url' => $id])
                    ->orWhere(['id' => $id]);
            })
            ->where(['is_del' => 0])
            ->first();
        if (!$blog) {
            return view('Web.404', [
                'message' => '文章信息不存在！'
            ]);
        } else {
            return view('Web.blog', [
                'blogTitle' => env('BLOG_TITLE'),
                'title' => $blog->title,
                'contentHtml' => $blog->content_html
            ]);
        }
    }

    public function page($url)
    {
        $page = DB::table(\App\Db\Page::PAGE)->select()->where(['url' => $url, 'is_del' => 0])->first();
        if (!$page) {
            return view('Web.404', [
                'message' => '页面不存在！'
            ]);
        } else {
            return view('Web.page', [
                'blogTitle' => env('BLOG_TITLE'),
                'title' => $page->title,
                'contentHtml' => $page->content_html
            ]);
        }
    }
}
