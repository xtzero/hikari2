<?php

namespace App\Http\Middleware;

use App\Db\AdminPassword;
use Illuminate\Http\Request;

class VerifyToken
{
    public function handle(Request $request, \Closure $next)
    {
        $token = $request->input('token');
        if (!$token) {
            die(json_encode([
                'code' => 10001,
                'msg' => '请重新登录',
                'data' => ''
            ]));
        }
        if (AdminPassword::getToken() !== $token) {
            die(json_encode([
                'code' => 10001,
                'msg' => '请重新登录!',
                'data' => ''
            ]));
        }

        self::sign($request->input(null));
        return $next($request);
    }

    public static function sign($params)
    {
//        if (!$params['_']) {
//            die(json_encode([
//                'code' => 10002,
//                'msg' => '签名失败！',
//                'data' => ''
//            ]));
//        }
//        if (time() - $params['_'] > 60) {
//            die(json_encode([
//                'code' => 10003,
//                'msg' => '签名过期',
//                'data' => ''
//            ]));
//        }
//        if (!$params['sign']) {
//            die(json_encode([
//                'code' => 10004,
//                'msg' => '缺少签名！',
//                'data' => ''
//            ]));
//        }
//        $passSign = $params['sign'];
//        unset($params['sign']);
//        ksort($params);
//        $sign = md5($params['_']);
//        var_dump($params['_']);
//        var_dump($sign);
//        if ($passSign !== $sign) {
//            die(json_encode([
//                'code' => 10005,
//                'msg' => '签名错误！',
//                'data' => []
//            ]));
//        }
    }
}
