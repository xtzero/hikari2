const mix = require('laravel-mix');
const path = require("path");

mix.webpackConfig({
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'resources/views/Admin/'),
            '~': path.resolve(__dirname, 'resources/views/Admin/components/'),
            '#': path.resolve(__dirname, 'resources/views/Admin/components_wap/'),
        }
    }
})

mix.js('resources/js/app.js', 'public/js')
    .vue();
