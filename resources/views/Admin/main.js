const Vue = require('vue').default;

// 引入 antd
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
Vue.config.productionTip = false;
Vue.use(Antd);

// 引入 Vant
import Vant from 'vant'
import 'vant/lib/index.css'
Vue.use(Vant)

// 引入 vue-router
import VueRouter from 'vue-router';
Vue.use(VueRouter);

// 路由配置
import router from './router/index'

import App from './app.vue'

const app = new Vue({
    el: '#app',
    router: router,
    render: h => h(App)
});
