export function openBlogPage(id) {
    window.open(`/post/${id}`)
}

export function openPagePage(url) {
    window.open(`/p/${url}`)
}

export function getSourceUrl(id) {
    return window.location.href.split('#')[0].replace('admin', '') + 'source/' + id
}

export function appendUrl(method, url, data = {}) {
    const href = window.location.href.split('/')
    const apiHost = `${href[0]}//${href[2]}`
    method = method.toLocaleLowerCase();
    if (!['post', 'get'].includes(method)) {
        throw('不允许的请求类型')
    }
    
    data.token = localStorage.getItem('access_token') ?? ''
    switch (method) {
        case 'get': {
            return `${apiHost}/${url}?` + Object.keys(data).map(k => `${k}=${data[k]}`).join('&')
        } break
        case 'post': {
            return `${apiHost}/${url}`
        } break
    }
}