import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import { Dashboard, WapDashboard } from './modules/dashboard'
import { Blog, WapBlog} from './modules/blog'
import { Page, WapPage} from './modules/page'
import Others from './modules/others'
import { Setting, WapSetting } from './modules/setting'
import { Source } from './modules/source'

const routes = [
    {
        path: '/',
        component: () => import('~/menu/MainLayout.vue'),
        redirect: '/dashboard',
        children: [
            ...Dashboard,
            ... Blog,
            ...Page,
            ...Setting,
            ...Source
        ]
    },
    {
        path: '/wap',
        component: () => import('#/menu/WapLayout.vue'),
        redirect: '/wap/dashboard',
        children: [
            ...WapDashboard,
            ...WapBlog,
            ...WapPage,
            ...WapSetting
        ]
    },
    ...Others
]

const router = new VueRouter({
    routes
})

router.beforeEach((to, from, next) => {
    document.title = to.title ?? to.name ?? ''

    Vue.prototype.$changeTopMenu ? Vue.prototype.$changeTopMenu([]) : ''
    next();
});

export default router
