export const Setting = [
    {
        path: '/setting/baseinfo',
        name: '系统设置',
        component: () => import('~/pages/Setting/BaseSetting.vue'),
    },
    {
        path: '/setting/navSetting',
        name: '导航设置',
        component: () => import('~/pages/Setting/NavSetting.vue'),
    },
    {
        path: '/setting/bottomSetting',
        name: '页脚设置',
        component: () => import('~/pages/Setting/BottomSetting.vue'),
    },
    {
        path: '/setting/themeSetting',
        name: '主题设置',
        component: () => import('~/pages/Setting/ThemeSetting.vue'),
    },
    {
        path: '/setting/sysCategory',
        name: '系统分类设置',
        component: () => import('~/pages/Setting/SysCategory.vue'),
    },
    {
        path: '/setting/site',
        name: '站点管理',
        component: () => import('~/pages/Setting/SiteManage.vue'),
    },
    {
        path: '/setting/export',
        name: '数据备份',
        component: () => import('~/pages/Setting/ExportData.vue'),
    },
    {
        path: '/setting/import',
        name: '数据导入',
        component: () => import('~/pages/Setting/ImportData.vue'),
    },
    {
        path: '/setting/accessLog',
        name: '访问日志',
        component: () => import('~/pages/Setting/VisitLog.vue'),
    },
    {
        path: '/setting/about',
        name: '关于 Hikari',
        component: () => import('~/pages/Setting/About.vue'),
    },
]

export const WapBlogSettingChildren = [
    {
        path: '/wap/setting/baseinfo',
        name: '基础设置',
        component: () => import('#/pages/Setting/BaseSetting.vue'),
    },
    {
        path: '/wap/setting/navSetting',
        name: '导航设置',
        component: () => import('#/pages/Setting/NavSetting.vue'),
    },
    {
        path: '/wap/setting/bottomSetting',
        name: '页脚设置',
        component: () => import('#/pages/Setting/BottomSetting.vue'),
    },
    {
        path: '/wap/setting/themeSetting',
        name: '主题设置',
        component: () => import('#/pages/Setting/ThemeSetting.vue'),
    }
]
export const WapSystemSettingChildren = [
    {
        path: '/wap/setting/site',
        name: '站点管理',
        component: () => import('#/pages/Setting/SiteManage.vue'),
    },
    {
        path: '/wap/setting/export',
        name: '数据备份',
        component: () => import('#/pages/Setting/ExportData.vue'),
    },
    {
        path: '/wap/setting/import',
        name: '数据导入',
        component: () => import('#/pages/Setting/ImportData.vue'),
    },
    {
        path: '/wap/setting/accessLog',
        name: '访问日志',
        component: () => import('#/pages/Setting/VisitLog.vue'),
    },
    {
        path: '/wap/setting/about',
        name: '关于 Hikari',
        component: () => import('#/pages/Setting/About.vue'),
    }
]
export const WapSetting = [
    {
        path: '/wap/blogSetting',
        name: '博客设置',
        component: () => import('#/pages/Setting/BlogSettingLayout.vue'),
        children: WapBlogSettingChildren
    },
    {
        path: '/wap/systemSetting',
        name: '系统设置',
        component: () => import('#/pages/Setting/SystemSettingLayout.vue'),
        children: WapSystemSettingChildren
    }
]
