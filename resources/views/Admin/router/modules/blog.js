export const Blog = [
    {
        path: '/blog/blogs',
        name: '文章列表',
        component: () => import('~/pages/Blog/Blogs.vue')
    },
    {
        path: '/blog/new',
        name: '新博文',
        component: () => import('~/pages/Blog/Newblog.vue')
    },
    {
        path: '/blog/categorys',
        name: '分类列表',
        component: () => import('~/pages/Blog/Categorys.vue')
    }
]

export const wapblogChilds = [
    {
        path: '/wap/blog/blogs',
        name: '文章列表',
        component: () => import('#/pages/Blog/Blogs.vue')
    },
    {
        path: '/wap/blog/new',
        name: '新博文',
        component: () => import('#/pages/Blog/NewBlog.vue')
    },
    {
        path: '/wap/blog/categorys',
        name: '分类列表',
        component: () => import('#/pages/Blog/Categorys.vue')
    }
]

export const WapBlog = [
    {
        path: '/wap/blog',
        name: '文章管理',
        component: () => import('#/pages/Blog/BlogLayout.vue'),
        children: wapblogChilds
    }
]

