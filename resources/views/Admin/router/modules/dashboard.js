export const Dashboard =
[
    {
        path: '/dashboard',
        name: '控制面板',
        component: () => import('~/pages/Dashboard.vue')
    }
]
export const WapDashboard = [
    {
        path: '/wap/dashboard',
        name: '控制面板',
        component: () => import('#/pages/Dashboard.vue')
    }
]

export const WapDashboardChilds = [
    '/wap/dashboard'
]
