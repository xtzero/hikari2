export const Page = [
    {
        path: '/page/pages',
        name: '页面列表',
        component: () => import('~/pages/Page/Pages.vue')
    },
    {
        path: '/page/new',
        name: '新页面',
        component: () => import('~/pages/Page/Newpage.vue')
    }
]

export const WapPageChildren = [
    {
        path: '/wap/page/pages',
        name: '页面列表',
        component: () => import('#/pages/Page/Pages.vue')
    },
    {
        path: '/wap/page/new',
        name: '新页面',
        component: () => import('#/pages/Page/NewPage.vue')
    }
]
export const WapPage = [
    {
        path: '/wap/page',
        name: '页面管理',
        component: () => import('#/pages/Page/PageLayout.vue'),
        children: WapPageChildren
    }
]
