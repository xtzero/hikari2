import request from '../utils/request'

export function upload(data) {
    return request('post', 'upload', data)
}
export function getSourceList(data) {
    return request('get', 'admin/getSourceList', data)
}
export function deleteSource(data) {
    return request('post', 'admin/deleteSource', data)
}
