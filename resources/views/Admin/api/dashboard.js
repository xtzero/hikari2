import request from '../utils/request'

export function dashboard(data) {
    return request('get', 'admin/dashboard', data)
}
