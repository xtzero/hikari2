import request from '../utils/request'

export function getPageList(data) {
    return request('get', 'admin/getPageList', data)
}

export function addNewPage(data) {
    return request('post', 'admin/addNewPage', data)
}
export function getPageRow(data) {
    return request('get', 'admin/getPageRow', data)
}
export function savePage(data) {
    return request('post', 'admin/savePage', data)
}
export function deletePage(data) {
    return request('post', 'admin/deletePage', data)
}