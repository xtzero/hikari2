import axios from 'axios'
import request from '../utils/request'
import { appendUrl } from '../utils/url'

export function getSetting(data) {
    return request('get', 'admin/getSetting', data)
}

export function setSetting(data) {
    return request('post', 'admin/setSetting', data)
}
export function exportDbFile() {
    return appendUrl('get', 'admin/exportDbFile')
}
export function getVisitLogOptions() {
    return request('get', 'admin/getVisitLogOptions')
}
export function getVisitLog(data) {
    return request('get', 'admin/getVisitLog', data)
}
export function clearVisitLog(data) {
    return request('post', 'admin/clearVisitLog', data)
}
export function getAllThemesFromMarket(data) {
    return request('get', 'admin/getAllThemesFromMarket', data)
}
export function getThemeJson(data) {
    return request('get', 'admin/getThemeJson', data)
}
export function saveThemeJson(data) {
    return request('post', 'admin/saveThemeJson', data)
}
export function getSiteList(data) {
    return request('get', 'admin/getSiteList', data)
}
export function addSite(data) {
    return request('post', 'admin/addSite', data)
}
export function deleteSite(data) {
    return request('post', 'admin/deleteSite', data)
}