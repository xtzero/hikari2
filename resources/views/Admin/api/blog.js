import request from '../utils/request'

export function getCategoryList(data) {
    return request('get', 'admin/getCategoryList', data)
}

export function addCategory(data) {
    return request('post', 'admin/addCategory', data)
}

export function getBlogList(data) {
    return request('get', 'admin/getBlogList', data)
}

export function addNewBlog(data) {
    return request('post', 'admin/addNewBlog', data)
}
export function saveBlog(data) {
    return request('post', 'admin/saveBlog', data)
}

export function getBlogRow(data) {
    return request('get', 'admin/getBlogRow', data)
}
export function deleteBlog(data) {
    return request('post', 'admin/deleteBlog', data)
}
export function saveCategory(data) {
    return request('post', 'admin/saveCategory', data)
}
export function deleteCategory(data) {
    return request('post', 'admin/deleteCategory', data)
}