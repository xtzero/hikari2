/**
 * 左侧菜单数据
 *
 * 如果菜单项没有折叠的子菜单项，则设置 path、title、icon
 * 如果有子菜单项，则设置 title、icon、children
 *
 * 子菜单项需要设置 path、title、icon
 */
export default [
    {
        path: '/dashboard',
        title: '控制台',
        icon: 'dashboard'
    },
    {
        title: '博客管理',
        icon: 'file',
        children: [
            {
                path: '/blog/new',
                title: '新博文',
                icon: 'file-add'
            },
            {
                path: '/blog/blogs',
                title: '文章列表',
                icon: 'folder'
            },
            {
                path: '/blog/categorys',
                title: '分类列表',
                icon: 'hdd'
            }
        ]
    },
    {
        title: '页面管理',
        icon: 'file-text',
        children: [
            {
                path: '/page/new',
                title: '新页面',
                icon: 'file-add'
            },
            {
                path: '/page/pages',
                title: '页面列表',
                icon: 'folder'
            }
        ]
    },
    {
        title: '资源管理',
        icon: 'cloud',
        children: [
            {
                path: '/source/image',
                title: '图片资源管理',
                icon: 'picture'
            }
        ]
    },
    {
        title: '博客设置',
        icon: 'setting',
        children: [
            {
                path: '/setting/baseinfo',
                title: '基本信息设置',
                icon: 'info-circle'
            },
            {
                path: '/setting/navSetting',
                title: '导航设置',
                icon: 'menu'
            },
            {
                path: '/setting/bottomSetting',
                title: '页脚设置',
                icon: 'border-bottom'
            },
            {
                path: '/setting/themeSetting',
                title: '主题设置',
                icon: 'crown'
            },
            {
                path: '/setting/sysCategory',
                title: '系统分类设置',
                icon: 'build'
            },
        ]
    },
    {
        title: '系统设置',
        icon: 'tool',
        children: [
            {
                path: '/setting/site',
                title: '站点管理',
                icon: 'global'
            },
            // {
            //     path: '/setting/export',
            //     title: '数据备份',
            //     icon: 'download'
            // },
            // {
            //     path: '/setting/import',
            //     title: '数据导入',
            //     icon: 'import'
            // },
            {
                path: '/setting/accessLog',
                title: '访问日志',
                icon: 'project'
            },
            {
                path: '/setting/about',
                title: '关于 Hikari 2',
                icon: 'smile'
            }
        ]
    }
]
