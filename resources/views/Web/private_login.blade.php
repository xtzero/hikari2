<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>隐私登录</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/ant-design-vue@1.7.8/dist/antd.min.css">
    <script src="https://unpkg.com/vue@2.6/dist/vue.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/ant-design-vue@1.7.8/dist/antd.min.js"></script>
    <script>
        function setCookie(c_name,value,expiredays) {
            var exdate=new Date();
            exdate.setDate(exdate.getDate()+expiredays);
            document.cookie=c_name+ "=" +escape(value) + ((expiredays==null) ? "" : ";expires="+exdate.toGMTString())
        }
        function getCookie(c_name) {
            if (document.cookie.length>0) {
                c_start=document.cookie.indexOf(c_name + "=");
                if (c_start != -1) {
                    c_start=c_start + c_name.length+1
                    c_end=document.cookie.indexOf(";",c_start);
                    if (c_end == -1) c_end=document.cookie.length;
                    return unescape(document.cookie.substring(c_start,c_end))
                }
            }
            return ""
        }
    </script>
    <style>
        #app {
            width: 100vw;
            height: 100vh;
            overflow: hidden;
            background-color: #efeff5;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }
        #app .container {
            width: 90%;
            height: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }
        #app .container .title {
            font-size: 50px;
            margin-bottom: 50px;
        }
        #app .container .pwdinput {
            width: 30%;
        }
        #app .container .btn {
            width: 30%;
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <div id="app">
        <div class="container">
            <div class="title">Hikari 2 登录</div>
            <div class="pwdinput">
                <a-input v-model="password" placeholder="请输入访问密码"></a-input>
            </div>
            <div class="btn">
                <a-button type="primary" block @click="enter">进入</a-button>
            </div>
        </div>
    </div>
    <script>
        // 在 #app 标签下渲染一个按钮组件
        new Vue({
            el: '#app',
            data: {
                password: ''
            },
            mounted() {
                @if($msg)
                    antd.message.error('{{$msg}}')
                @endif
            },
            methods: {
                enter() {
                    setCookie('access_password', this.password, 1)
                    window.location.href = '/'
                }
            }
        })
    </script>
</body>
</html>
