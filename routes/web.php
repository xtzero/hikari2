<?php

use Illuminate\Support\Facades\Route;
// 公共
Route::name('上传资源')->post('/upload', [\App\Http\Controllers\Admin\Source::class, 'upload']);
Route::name('获取资源')->get('/source/{id}', [\App\Http\Controllers\Admin\Source::class, 'getSource']);

// 请帖前台，不用加载特定的路由，直接路由到一个控制器上，加载不同blade就行


// 博客前台
/*** 加载主题自定义的路由 */
$siteDbRes = \Illuminate\Support\Facades\DB::table('site')->where('is_del', 0)->where('host', 'like', "%,". ($_SERVER['HTTP_HOST'] ?? '') .",%")->first();
$themeDbRes = isset($siteDbRes->id) && $siteDbRes->id > 0 ? \Illuminate\Support\Facades\DB::table('setting')->where(['type' => 'theme', 'site' => $siteDbRes->id])->first() : [];
$themeSetting = $themeDbRes ? $themeDbRes->value : 'default';
$themePath = str_replace('routes', '', __DIR__) . 'storage/blogsrc/themes/' . $themeSetting;
$themeRoutePath = $themePath . '/route.php';
if (file_exists($themeRoutePath)) { try { require_once($themeRoutePath); } catch (\Exception $e) { die('主题引入错误：' . $themeSetting); } } else { die('主题不存在：' . $themeRoutePath); } app('view')->getFinder()->prependLocation($themePath);
/*** 加载主题自定义的路由 结束 */

// 后台
Route::view('/admin', 'Admin.index');
// 后台的免登陆接口
Route::prefix('/admin')->group(function() {
    Route::name('后台登录')->post('/login', [\App\Http\Controllers\Admin\Login::class, 'login']);
});
// 后台常规接口
Route::prefix('/admin')->middleware('verify_token')->group(function() {
    Route::name('添加分类')->post('/addCategory', [\App\Http\Controllers\Admin\Blog::class, 'addCategory']);
    Route::name('获取分类')->get('/getCategoryList', [\App\Http\Controllers\Admin\Blog::class, 'getCategoryList']);
    Route::name('新建文章')->post('/addNewBlog', [\App\Http\Controllers\Admin\Blog::class, 'addNewBlog']);
    Route::name('文章列表')->get('/getBlogList', [\App\Http\Controllers\Admin\Blog::class, 'getBlogList']);
    Route::name('新建页面')->post('/addNewPage', [\App\Http\Controllers\Admin\Page::class, 'addNewPage']);
    Route::name('页面列表')->get('/getPageList', [\App\Http\Controllers\Admin\Page::class, 'getPageList']);
    Route::name('编辑文章')->post('/saveBlog', [\App\Http\Controllers\Admin\Blog::class, 'saveBlog']);
    Route::name('编辑页面')->post('/savePage', [\App\Http\Controllers\Admin\Page::class, 'savePage']);
    Route::name('编辑分类')->post('/saveCategory', [\App\Http\Controllers\Admin\Blog::class, 'saveCategory']);
    Route::name('删除文章')->post('/deleteBlog', [\App\Http\Controllers\Admin\Blog::class, 'deleteBlog']);
    Route::name('删除分类')->post('/deleteCategory', [\App\Http\Controllers\Admin\Blog::class, 'deleteCategory']);
    Route::name('删除页面')->post('/deletePage', [\App\Http\Controllers\Admin\Page::class, 'deletePage']);
    Route::name('获取一条文章')->get('/getBlogRow', [\App\Http\Controllers\Admin\Blog::class, 'getBlogRow']);
    Route::name('获取一条页面')->get('/getPageRow', [\App\Http\Controllers\Admin\Page::class, 'getPageRow']);
    Route::name('获取所有配置')->get('/getSetting', [\App\Http\Controllers\Admin\Setting::class, 'getSetting']);
    Route::name('保存配置')->post('/setSetting', [\App\Http\Controllers\Admin\Setting::class, 'setSetting']);
    Route::name('下载数据库文件')->get('/exportDbFile', [\App\Http\Controllers\Admin\Setting::class, 'exportDbFile']);
    Route::name('获取访问记录选项')->get('/getVisitLogOptions', [\App\Http\Controllers\Admin\Setting::class, 'getVisitLogOptions']);
    Route::name('获取访问记录')->get('/getVisitLog', [\App\Http\Controllers\Admin\Setting::class, 'getVisitLog']);
    Route::name('删除访问记录')->post('/clearVisitLog', [\App\Http\Controllers\Admin\Setting::class, 'clearVisitLog']);
    Route::name('从主题市场获取所有主题')->get('/getAllThemesFromMarket', [\App\Http\Controllers\Admin\Setting::class, 'getAllThemesFromMarket']);
    Route::name('获取主题的json配置')->get('/getThemeJson', [\App\Http\Controllers\Admin\Setting::class, 'getThemeJson']);
    Route::name('保存主题的json配置')->post('/saveThemeJson', [\App\Http\Controllers\Admin\Setting::class, 'saveThemeJson']);
    Route::name('获取站点配置')->get('/getSiteList', [\App\Http\Controllers\Admin\Setting::class, 'getSiteList']);
    Route::name('添加站点配置')->post('/addSite', [\App\Http\Controllers\Admin\Setting::class, 'addSite']);
    Route::name('删除站点配置')->post('/deleteSite', [\App\Http\Controllers\Admin\Setting::class, 'deleteSite']);
    Route::name('获取控制台数据')->get('/dashboard', [\App\Http\Controllers\Admin\Dashboard::class, 'index']);
    Route::name('获取资源列表')->get('/getSourceList', [\App\Http\Controllers\Admin\Source::class, 'getSourceList']);
    Route::name('删除资源')->post('/deleteSource', [\App\Http\Controllers\Admin\Source::class, 'deleteSource']);
    Route::name('发布心情')->post('/sendMoment', [\App\Http\Controllers\Admin\Moment::class, 'sendMoment']);
    Route::name('心情列表')->get('/getMomentList', [\App\Http\Controllers\Admin\Moment::class, 'getMomentList']);
    Route::name('删除心情')->post('/deleteMoment', [\App\Http\Controllers\Admin\Moment::class, 'deleteMoment']);
});
Route::name('新建邀请')->middleware('check_params')->post('/addInvite', [\App\Http\Controllers\Admin\Invite::class, 'addInvite']);
Route::name('修改邀请')->middleware('check_params')->post('/editInvite', [\App\Http\Controllers\Admin\Invite::class, 'editInvite']);

