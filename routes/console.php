<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

Artisan::command('reset-admin-pwd', function() {
    \App\Http\Services\Admin\LoginService::resetPwd();
});

Artisan::command('init-db', function() {
    echo '开始初始化数据库...' . PHP_EOL;
    \App\Db\Init::handle();
    echo '初始化数据库完毕...' . PHP_EOL;
});

Artisan::command('init-site', function () {
    \App\Http\Services\Admin\SiteService::init();
});
